import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Users from "./components/home/Users";
import Posts from "./components/posts/Posts";
import Post from "./components/posts/Post";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      component: Home,
      children: [
        {
          path: "",
          name: "users",
          component: Users
        },
        {
          path: "user/:userId/post",
          name: "user-posts",
          component: Posts
        },
        {
          path: "post/:postId",
          name: "post",
          component: Post
        }
      ]
    }
  ]
});
